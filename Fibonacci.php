<?php

$lim = 100;

$cnt = $lim;
if ($argc >= 2) {
  $cnt = min(max($argv[1], 1), $lim);
}

echo "Element count: " . $cnt . "\n";
echo "Hit Ctrl-C to terminate...\n";

$fp = fopen("/tmp/fibonacci.txt", "w+");

while (1) {
  $a = array(0, 1);
  for ($i = 0; $i < $cnt; $i++) {
    if (flock($fp, LOCK_EX)) {
      if ($i == 0) {
        ftruncate($fp, 0);
        rewind($fp);
        fwrite($fp, "0\n");
      } else if ($i == 1) {
        fwrite($fp, "1\n");
      } else {
        $c = $a[0] + $a[1];
        fwrite($fp, $c . "\n");
        $a[0] = $a[1];
        $a[1] = $c;
      }
      flock($fp, LOCK_UN);
    } else {
      echo "couldn't get writer lock";
    }
  sleep(1);
  }
}

fclose($fp);
