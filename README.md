## Fibonacci Sequence Generator

This application will generate the first N elements of the Fibonacci
sequence and put them in a temporary file.  The elements are generated
once a second and appended to the temporary file.  The file resets and
repeats after the Nth element.  The application asserts
a file lock before it writes the file so other applications can
synchronously read the file.

You can view the sequence in a web page using [this](https://gitlab.com/tylerjbrooks/fibonacci-web) project.

### Installation

```
git clone https://gitlab.com/tylerjbrooks/fibonacci-gen.git
```

### Build Notes

This is a php script so you don't build it.

### Usage

Run the php script like this (N=60 as default):
```
php Fibonacci.php
```

Run the php script like this to specify N:
```
php Fibonacci.php 10
```
This will generate 10 elements of the sequence and then repeat.

You stop the script by hitting Cntl-C.

### Discussion

Simple application.  It sits in a loop forever computing the next element of the Fibonacci sequence.
It asserts an exclusive lock on a file (i.e. flock($file, LOCK_EX)) and then appends the next element
to the end of the file.

### Notes

None

### To Do

None
